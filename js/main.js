var fireflies = 200;
var $container = $(".container");
var $containerWidth = $container.width();
var $containerHeight = $container.height();
var master = new TimelineMax();

for (var i = 0; i < fireflies; i++) {
  var firefly = $('<div class="firefly"></div>');
  TweenLite.set(firefly, {
    x: Math.random() * $containerWidth,
    y: Math.random() * $containerHeight
  });
  $container.append(firefly);
  flyTheFirefly(firefly);
}

function flyTheFirefly(elm) {
  var flyTl = new TimelineMax();
  var fadeTl = new TimelineMax({
    delay: Math.floor(Math.random() * 2) + 1,
    repeatDelay: Math.floor(Math.random() * 6) + 1,
    repeat: -1
  });

  fadeTl.to(
    [elm],
    0.25,
    { opacity: 0.25, yoyo: true, repeat: 1, repeatDelay: 0.2, yoyo: true },
    Math.floor(Math.random() * 6) + 1
  );

  flyTl
    .set(elm, {scale: Math.random() * 0.75 + 0.5})
    .to(elm, Math.random() * 100 + 100, {
    bezier: {
      values: [
        {
          x: Math.random() * $containerWidth,
          y: Math.random() * $containerHeight
        },
        {
          x: Math.random() * $containerWidth,
          y: Math.random() * $containerHeight
        }
      ]
    },
    onComplete: flyTheFirefly,
    onCompleteParams: [elm]
  });
}




/********************************************************
calendar ************************************************
/*******************************************************/
var clearChecker = true;
var list1 = false;
var isEvent = false;
$(document).ready(function(){

  $('.calendar-picker').fullCalendar({
    height: 500,
    header:{
      left: 'prev',
      center: 'title',
      right: 'next'
    },
    //locale: 'zh-cn',
    eventSources:eventsToExport,

    eventClick:  function(event, jsEvent, view) {
             changeModal(event);

         },
     eventRender:function(event, element,view) {
       var curMonth = getCurrentMonth()
       curMonth += 1;
       var num = event.start._i
       num = num.slice(5,7).replace(/^0+/, '')
       num = parseInt(num)
       changeToChinese();
       changeEvent();
       if(curMonth == num){
         isEvent = true;
         showEvents(event)
       }

  },
      viewRender:function(view,element){
        clearEvent();

      }

  });


$('.fc-prev-button,.fc-next-button').on('click',function(){
  //checkEvent();
  changeToChinese();
  changeEvent();
})

changeEvent();
currentTime(new Date());
setInterval(function(){
  currentTime(new Date());
},1000)

//helpers ************************************

  function getCurrentMonth(){
    var date = $(".calendar-picker").fullCalendar('getDate');
    var month_int = date;
    return parseInt(month_int._i[1])
  }

  function changeModal(event){
    var day = getDay(event.start)

    $('.modal-description').html(event.description);
    $('.modal-dayNum').html(day.dayNum);
    $('.modal-dayText').html(day.dayText);
    $('.modal-monthChinese').html(day.month + '月');
    $('.modal-dayChinese').html(day.dayNum+ '日');
    $('.button-redirect').attr('href',event.redirectURL);
    $('#calendarModal').modal();
  }

  function changeToChinese(){
    $('.fc-widget-header.fc-sun span').html('周一')
    $('.fc-widget-header.fc-mon span').html('周二')
    $('.fc-widget-header.fc-tue span').html('周三')
    $('.fc-widget-header.fc-wed span').html('周四')
    $('.fc-widget-header.fc-thu span').html('周五')
    $('.fc-widget-header.fc-fri span').html('周六')
    $('.fc-widget-header.fc-sat span').html('周日')
  }
})

  function changeEvent(){
    $('.fc-day-grid-event .fc-title').html('&nbsp;')
  }

  function getDay(str){
      var obj = {},
          strI = str._i
      obj.dayNum = strI.slice(-2).replace(/^0+/, '');
      obj.dayText = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][(new Date(strI)).getDay()];
      obj.month = strI.slice(5,7).replace(/^0+/, '')
      return obj
  }

  function currentTime(date){
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    var month = date.toString().slice(4,7);
    var week = date.toString().slice(0,4);
    var day = date.getDate();
    var year = date.getFullYear();

    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    seconds = seconds < 10 ? '0'+seconds : seconds;
    var strTime = hours + ':' + minutes + ':' + seconds;
    var strDay = month + " " + day + " " + year + ", " + week;

      $('.time-latest').html(strTime)
      $('.time-periods').html(ampm)
      $('.currentDay').html(strDay)
    }

    function clearEvent(){
        list1 = false;
        if(!isEvent){
          $('.events-list').html("")
        }
        else{
          isEvent = false
        }
    }

    function showEvents(events){
        if(!list1){
          $('.events-list').html("")
        }
        var num = events.start._i
         num = num.slice(-2).replace(/^0+/, '');
            list1 = "<tr>"+
                          "<td>" +
                            "<div class='event-date'>" + num +"</div>" +
                          "</td>" +
                          "<td>" +
                            "<div class='event-title'>" + events.title + "</div>" +
                          "</td>"
                        "</tr>"
             $('.events-list').append(list1)

    }


  //function
//end calendar
