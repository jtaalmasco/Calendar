var eventsToExport =  [
{
  events: [
    {
      title  : '事件 01',
      start  : '2018-06-01',
      end2  : '2018-06-01',
      description : '8周年 感恩有你',
      redirectURL : 'https://www.google.com/1'
    },
    {
      title  : '事件 02',
      start  : '2018-06-18',
      end2    : '2018-06-20',
      description : '8周年 感恩有你2',
      redirectURL : 'https://www.google.com/2'
    },
    {
      title  : '事件 03',
      start  : '2018-07-01',
      end2  : '2018-07-01',
      description : '8周年 感恩有你3',
      redirectURL : 'https://www.google.com/3'
    },
    {
      title  : '事件 04',
      start  : '2018-07-05',
      end2  : '2018-07-05',
      description : '8周年 感恩有你4',
      redirectURL : 'https://www.google.com/4'
    },
    {
      title  : '事件 05',
      start  : '2018-08-05',
      end2  : '2018-08-05',
      description : '8周年 感恩有你4',
      redirectURL : 'https://www.google.com/5'
    },
  ],

}]
